%global srcname requests-ftp

Name:		python-requests-ftp
Version:	0.3.1
Release:	18
Summary:	FTP Transport Adapter for Requests
License:	ASL 2.0
URL:		https://github.com/Lukasa/requests-ftp
Source0:	https://github.com/Lukasa/requests-ftp/archive/v%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python3-devel python3-setuptools

Patch1:         PR28-01-Adding-2-tests-and-updated-statud_code-build.patch
Patch2:         PR28-02-Adding-code-3-to-retr4ieve-status_code.patch
Patch3:         PR28-03-fix-warning-in-interpreting-ftp-status-codes-minor-d.patch
Patch5:         PR28-05-Improve-logging-in-status-code-extraction.patch 

%description
Requests-FTP is an implementation of a very stupid FTP transport adapter 
for use with the awesome Requests Python library.

%package     -n python3-%{srcname}
Summary:        python3 for requests-ftp
Requires:       python3-requests
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{srcname}
python3 for requests-ftp.

%package_help

%prep
%autosetup -n %{srcname}-%{version} -p1
rm -rf requests_ftp.egg-info

%build
%py3_build

%install
%py3_install

%files       -n python3-%{srcname}
%defattr(-,root,root)
%license LICENSE
%{python3_sitelib}/*

%files          help
%defattr(-,root,root)
%doc README.rst

%changelog
* Wed Oct 26 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 0.3.1-18
- Rebuild for next release

* Fri Oct 30 2020 jinzhimin <jinzhimin2@huawei.com> - 0.3.1-17
- remove python2-requests-ftp subpackage

* Thu Sep 10 2020 chengguipeng<chengguipeng1@huawei.com> - 0.3.1-16
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify source0 url

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.3.1-15
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:optimization the spec

* Sat Oct 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.3.1-14
- Package init
